## Triangle of Summaries

"Like Triangle of Sadness but with less emesis."

Let's say you have a big document (800+ pages) and you want to use a GPT-like LLM to review the document. For example, an omnibus appropriations bill before Congress. Perhaps you are in the widget-making business and you want to understand what the impact of the proposed legislation would be on your industry. So you make a prompt for the LLM that goes something like "You are an expert in the field of widgets. Please review the following proposed legislation and assess the impact on the widget industry; (entire text goes here).  

The problem however is that the LLM has a limited context window - perhaps 2k tokens, usually 4k, sometimes 8k, and if you're lucky, 32k. Even with the 32k window, although it is enormous, it still probably isn't enough to accommodate the entire text, which can be hundreds to thousands of pages. If you want your simulated expert to review this text, they will need to do it in chunks.  

The issue with simply splitting the text into (total length)/(window length) chunks is that the LLM will not be considering the context of each chunk.  

So instead, what you might do is make your chunks smaller, but pad each chunk with pre-chunk context and post-chunk context. In other words, generate a summary of everything before the chunk (such that the summary fits inside the pre-chunk space), and then repeat this step, but for everything after the chunk. Then assemble your prompt: instructions + pre-chunk-summary + chunk + post-chunk-summary.  

How should you generate the summaries? I am sure there are many possible ways, but here is the strategy I am trying. Split the original text into sections that more-or-less follow the divisions within the document, say 500 words or so per section. This will also be your Chunk size. Then instruct your LLM to summarize each section in a single sentence.  

Next, take the output of the previous step, and slide a window of size 2 down the list. For each pair, ask the LLM to summarize it in a single sentence. The result should be a new list of sentences whose length is 1 sentence shorter than what you started with. Take this new list and repeat the process. And again and again, until only a single sentence remains (save the outputs along the way)!

The advantage here is that now you can make summaries of arbitrary granularity, that emphasize some areas more than others. If you wanted (and were willing to spend the time/money), you could have made your initial section/chunk size 50 words/2 sentences instead of 500 words. Then you would really have access to every level of granularity.  

So once you have generated your "triangle of summaries" (basically like Triangle of Sadness, but with less emesis), you can assign a "relevance score" to every sentence in your triangle. One way to do this would be to compare every sentence in your Chunk to every sentence in your Triangle (except for the ones that summarize your Chunk - call this set the Occluded Triangle). For each sentence in your Chunk, there will be a "best hit" from your Occluded Triangle - ie the query and the hit would comprise a pair of sentences whose embeddings are more closely aligned than for any other pair. So, whichever sentence in your Occluded Triangle was the best hit, increment its relevance score by 1. Now repeat for the remaining sentences in your Chunk.  

Next, assemble your pre-chunk summary. Start with the deepest pre-chunk sentence in the occluded triangle. Hopefully, it is smaller than the available pre-chunk space; if it isn't then I guess you might need to redo the triangle with a smaller chunk size. Assuming that isn't an issue, you'll probably have the opposite one - unused pre-chunk space. How should it be allocated? Seems to me you would want to compare the relevance scores of your summary sentence's parents with the relevance score of your summary sentence. If score(parentA) + score(parentB) > 2* score(child summary sentence), and the length does not exceed the available pre-chunk space, then it would make sense to replace the child with its parents, because it will make the context both more detailed and more relevant to the Chunk. And you can just repeat this process for both of the parents, and so on, until you run out of room. You might have a scenario in which the parents are not any more relevant than the child, but the grandparents are, so you'd want to code for that too I think.  

Anyway, the code in the ipynb file will make the triangle.



## Update

This Triangle of Summaries really is a Triangle of Sadness. The experiment has produced a large number of increasingly inaccurate summaries. Although amusing, they are probably not very useful. It is like playing a game of telephone.  

I do think that the general principle is reasonable but will need to reconfigure it. Instead of making summaries of summaries, we should avoid that as much as possible. Rather, we should be summarizing texts of increasing scope AND size. So instead of only summarizing 2 sentences at a time, do:  

        1 2  
        1 2 3  
        1 2 3 4  
        1 2 3 4 5  


then move the window down by 1 and do:  

        2 3  
        2 3 4  
        2 3 4 5


and so on.

